# Rap Generation

В данном репозитории содержится код 4-х моделей для генерации текста и реп треков, в частности:
1) Vanilla RNN
2) Bidirectional LSTM
3) Transformer
4) Open AI GPT-2 (notebook version)

Гайд по запуску обучения и инференса каждой из моделей хранится в */Notebooks/textgen_guide.ipynb*

Также у каждой модели есть CLI, описание параметров, к примеру, для RNN, можно получить через:
`python -m RNN_model --help`
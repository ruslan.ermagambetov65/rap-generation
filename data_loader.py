import pandas as pd
import numpy as np
import collections

class DataLoader:
    def __init__(self, data_path, batch_size=64, sequence_length=64):
        self.batch_size = batch_size
        self.sequence_length = sequence_length
        with open(data_path, 'r', encoding='utf8', errors='ignore') as f:
            texts = f.read()
        count_pairs = sorted(collections.Counter(texts).items(), key=lambda x: -x[1])
        chars, _ = zip(*count_pairs)
        self.vocabulary_size = len(chars)
        self.vocabulary = dict(zip(chars, range(len(chars))))
        self.id2vocabulary = {v: k for k, v in self.vocabulary.items()}
        self.tensor = np.array(list(map(self.vocabulary.get, texts)))
        self.n_batches = int(self.tensor.size / (self.batch_size * self.sequence_length))
        self.tensor = self.tensor[:self.n_batches*self.batch_size*self.sequence_length]

class BatchGenerator:
    def __init__(self, tensor):
        self.inputs = tensor
        self.targets = np.copy(tensor)
        self.targets[:-1] = self.inputs[1:]
        self.targets[-1] = self.inputs[0]

    def generate_batches(self, n_batches, batch_size=64):
        input_batches = np.split(self.inputs.reshape(batch_size, -1), n_batches, 1)
        target_batches = np.split(self.targets.reshape(batch_size, -1), n_batches, 1)
        while True:
            for batch in range(n_batches):
                yield input_batches[batch], target_batches[batch]

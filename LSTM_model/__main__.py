from .train_generate import train_main, generate_main
import click

@click.command()
@click.option('--train_or_gen', default='train', help='Train or Generate')
@click.option('--train_text_file', default='.', help='Source text save path')
@click.option('--train_save_path', default='.', help='Trained models save path')
@click.option('--train_n_epochs', default=2000, help='number of epochs to train')
@click.option('--train_save_every', default=200, help='number of iterations to save')
@click.option('--gen_text_file', default='.', help='path to source text to decode generated')
@click.option('--gen_model_weights_file', default='.', help='path to model_weights')
@click.option('--gen_prefix', default='Motherfucker', help='prefix of generated')
@click.option('--gen_length', default=512, help='length of generated')
@click.option('--gen_temperature', default=.7, help='temperature of generated')
def command_line_api(
         train_or_gen,
         train_text_file,
         train_save_path,
         train_n_epochs,
         train_save_every,
         gen_text_file,
         gen_model_weights_file,
         gen_prefix,
         gen_length,
         gen_temperature
                     ):
    if train_or_gen == 'train':
        train_main(text_path=train_text_file, save_path=train_save_path,
                   num_epochs=train_n_epochs, save_every=train_save_every)
    else:
        generated = generate_main(text_path = gen_text_file, model_weights_path=gen_model_weights_file,
                      prefix=gen_prefix, length=gen_length, temperature=gen_temperature)
        click.echo(generated)

if __name__ == "__main__":
    command_line_api()
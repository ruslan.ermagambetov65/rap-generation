import torch
from torch import nn, optim
from torch.autograd import Variable
from torch.nn import functional as F

import numpy as np

class Model(nn.Module):
    """
    build character embeddings LSTM text generation model.
    """

    def __init__(self, vocab_size, embedding_size=32,
                 rnn_size=128, num_layers=2, drop_rate=0.0, device='cpu'):
        super(Model, self).__init__()
        self.args = {"vocab_size": vocab_size, "embedding_size": embedding_size,
                     "rnn_size": rnn_size, "num_layers": num_layers,
                     "drop_rate": drop_rate, "device": 'cpu'}
        self.encoder = nn.Embedding(vocab_size, embedding_size)
        self.dropout = nn.Dropout(drop_rate)
        self.rnn = nn.LSTM(embedding_size, rnn_size, num_layers, dropout=drop_rate)
        self.decoder = nn.Linear(rnn_size, vocab_size)

    def forward(self, inputs, state):
        # input shape: [seq_len, batch_size]
        embed_seq = self.dropout(self.encoder(inputs))
        # shape: [seq_len, batch_size, embedding_size]
        rnn_out, state = self.rnn(embed_seq, state)
        # rnn_out shape: [seq_len, batch_size, rnn_size]
        # hidden shape: [2, num_layers, batch_size, rnn_size]
        rnn_out = self.dropout(rnn_out)
        # shape: [seq_len, batch_size, rnn_size]
        logits = self.decoder(rnn_out.view(-1, rnn_out.size(2)))
        # output shape: [seq_len * batch_size, vocab_size]
        return logits, state

    def predict(self, input, hidden):
        # input shape: [seq_len, batch_size]
        logits, hidden = self.forward(input, hidden)
        # logits shape: [seq_len * batch_size, vocab_size]
        # hidden shape: [2, num_layers, batch_size, rnn_size]
        probs = F.softmax(logits, dim=1)
        # shape: [seq_len * batch_size, vocab_size]
        probs = probs.view(input.size(0), input.size(1), probs.size(1))
        # output shape: [seq_len, batch_size, vocab_size]
        return probs, hidden

    def init_state(self, batch_size=1):
        """
        initialises rnn states.
        """
        return (
        Variable(torch.zeros(self.args["num_layers"], batch_size, self.args["rnn_size"])).to(self.args["device"]),
        Variable(torch.zeros(self.args["num_layers"], batch_size, self.args["rnn_size"])).to(self.args["device"]))

    def save(self, checkpoint_path="model.pt"):
        """
        saves model and args to checkpoint_path.
        """
        checkpoint = {"args": self.args, "state_dict": self.state_dict()}
        torch.save(checkpoint, checkpoint_path)

    @classmethod
    def load(cls, checkpoint_path):
        """
        loads model from checkpoint_path.
        """
        with open(checkpoint_path, "rb") as f:
            checkpoint = torch.load(f)
        model = cls(**checkpoint["args"])
        model.load_state_dict(checkpoint["state_dict"])
        return model

    def sample(self, preds, temperature, device):
        if device == 'cpu':
            preds = preds.squeeze().detach().numpy()
            preds = np.asarray(preds).astype('float64')  # / temperature
            preds = np.log(preds) / temperature
            exp_preds = np.exp(preds)
            preds = exp_preds / np.sum(exp_preds)
            probas = np.random.multinomial(1, preds, 1)
            return torch.tensor([np.argmax(probas)])
        else:
            preds = preds.squeeze()
            temperature = torch.tensor(temperature)
            preds = torch.log(preds) / temperature
            # preds = preds / temperature
            exp_preds = torch.exp(preds)
            preds = exp_preds / torch.sum(exp_preds)
            probas = torch.multinomial(preds, 1)
            return probas

    def generate_text(self, dataloader, seed, length=512, temperature=.5):
        """
        generates text of specified length from trained model
        with given seed character sequence.
        """

        generated = seed
        encoded = np.array(list(map(dataloader.vocabulary.get, seed)))
        encoded = Variable(torch.from_numpy(encoded)).to(torch.int64).to(self.args['device'])
        self.eval()

        x = encoded[:-1].unsqueeze(1)

        # input shape: [seq_len, 1]
        state = self.init_state()
        # get rnn state due to seed sequence
        _, state = self.predict(x, state)

        next_index = encoded[-1:]
        for i in range(length):
            x = next_index.unsqueeze(1)
            # input shape: [1, 1]
            probs, state = self.predict(x, state)
            # output shape: [1, 1, vocab_size]
            next_index = self.sample(probs, temperature=temperature, device=self.args['device'])
            # append to sequence
            generated += dataloader.id2vocabulary[int(next_index.data[0])]

        return generated
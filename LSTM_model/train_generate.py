import numpy as np
import time
from tqdm import tqdm

import torch
from torch import nn, optim
from torch.autograd import Variable
from torch.nn import functional as F

from data_loader import DataLoader, BatchGenerator
from .lstm_model import Model

def train_main(text_path, save_path, num_epochs=2000, save_every=200):

    DL = DataLoader(text_path)
    BG = BatchGenerator(DL.tensor).generate_batches(DL.n_batches, batch_size=DL.batch_size)

    if torch.cuda.is_available():
        from torch.cuda import FloatTensor, LongTensor
        DEVICE = torch.device('cuda')
    else:
        from torch import FloatTensor, LongTensor
        DEVICE = torch.device('cpu')

    model = Model(vocab_size=DL.vocabulary_size, device=DEVICE).to(DEVICE)

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=0.01)

    state = model.init_state(DL.batch_size)
    state = tuple([Variable(var.data) for var in state])

    seed = 'Motherfucker'

    for i in tqdm(range(num_epochs)):
        epoch_losses = torch.Tensor(DL.n_batches).to(DEVICE)
        time_epoch = time.time()
        model.train()

        for j in range(DL.n_batches):
            state = model.init_state(DL.batch_size)

            x, y = next(BG)
            x = Variable(torch.from_numpy(x).to(torch.int64)).t().to(DEVICE)
            y = Variable(torch.from_numpy(y).to(torch.int64)).t().contiguous().to(DEVICE)

            optimizer.zero_grad()

            logits, state = model.forward(x, state)
            loss = criterion(logits, y.view(-1))

            epoch_losses[j] = float(loss.data)
            # calculate gradients
            loss.backward()
            # clip gradient norm
            nn.utils.clip_grad_norm_(model.parameters(), 5)
            # apply gradient update
            optimizer.step()

        duration_epoch = time.time() - time_epoch
        print("epoch: %s, duration: %ds, loss: %.6g." %
              (i + 1, duration_epoch, epoch_losses.mean()))
        print(model.generate_text(DL, seed, length=512, temperature=.7))
        if (i + 1) % save_every == 0:
            model.save(save_path + f'/model_{i + 1}.pt')

    return model

def generate_main(text_path, model_weights_path, prefix = 'Fuck', length=512, temperature=.7):

    DL = DataLoader(text_path)
    model = Model.load(model_weights_path)

    return model.generate_text(DL, prefix, length=length, temperature=temperature)
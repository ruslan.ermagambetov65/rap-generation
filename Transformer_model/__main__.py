from .train_generate_Transformer import train_main, generate_main
import click

@click.command()
@click.option('--train_or_gen', default='train', help='Train or Generate')
@click.option('--text_file', default='.', help='Source text save path')
@click.option('--train_text_file_bpe', default='.', help='Train texts for be save path')
@click.option('--train_save_path', default='.', help='Trained models save path')
@click.option('--bpe_model_file', default='.', help='path to bpe_model')
@click.option('--gen_model_weights_file', default='.', help='path to model_weights')
@click.option('--gen_bpe_model_file', default='.', help='path to bpe model')
@click.option('--gen_prefix', default='Motherfucker', help='prefix of generated')
@click.option('--gen_length', default=512, help='length of generated')
def command_line_api(
         train_or_gen,
         text_file,
         train_text_file_bpe,
         train_save_path,
         bpe_model_file,
         gen_model_weights_file,
         gen_bpe_model_file,
         gen_prefix,
         gen_length
                     ):
    if train_or_gen == 'train':
        train_main(
           text_path=text_file,
           TRAIN_TEXTS_FILENAME=train_save_path,
           BPE_MODEL_NAME=bpe_model_file,
           model_weights_filename=train_save_path)
    else:
        greedy_generator = generate_main(
              model_weights_filename = gen_model_weights_file,
              BPE_MODEL_NAME=gen_bpe_model_file
        )

        click.echo(greedy_generator(gen_prefix, max_steps_n=gen_length))

if __name__ == "__main__":
    from torch import nn
    import torch

    from .transformer_model import LanguageModel, BatchFirstTransformerEncoder
    command_line_api()
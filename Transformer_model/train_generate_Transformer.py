import numpy as np
import youtokentome as yttm

from torch import nn
import torch

from .transformer_model import LanguageModel, BatchFirstTransformerEncoder, GreedyGenerator
from .transformer_dataloader import LanguageModelDataset
from .transformer_helpers import lm_cross_entropy, lr_scheduler
from .transformer_helpers import train_eval_loop

def load_corpus(fname, chunk_size=200):
    with open(fname, 'r', encoding='utf-8') as fin:
        full_text = fin.read()
    return [full_text[start:start + chunk_size] for start in range(0, len(full_text), chunk_size // 2)]

def save_texts_to_file(texts, out_file):
    with open(out_file, 'w', encoding='utf-8') as outf:
        outf.write('\n'.join(texts))

def train_main(text_path,
               TRAIN_TEXTS_FILENAME,
               BPE_MODEL_NAME,
               model_weights_filename='./transformer_weights.pth'):
    texts = load_corpus(text_path)

    np.random.shuffle(texts)

    TRAIN_SPLIT = int(len(texts) * 0.7)
    train_texts = texts[:TRAIN_SPLIT]
    test_texts = texts[TRAIN_SPLIT:]

    print('Размер обучающей выборки', len(train_texts))
    print('Размер валидационной выборки', len(test_texts))
    # saves train_texts to file for bpe to train on it
    save_texts_to_file(train_texts, TRAIN_TEXTS_FILENAME)

    yttm.BPE.train(data=TRAIN_TEXTS_FILENAME, vocab_size=1000, model=BPE_MODEL_NAME);
    tokenizer = yttm.BPE(BPE_MODEL_NAME)
    # encode train and test samples
    train_token_ids = tokenizer.encode(train_texts, bos=False, eos=False)
    test_token_ids = tokenizer.encode(test_texts, bos=False, eos=False)

    unknown_subwords_in_test = sum(1 for text in test_token_ids for token_id in text if token_id == 1)
    print('Количество случаев с неизвестными n-граммами символов в валидационной выборке',
          unknown_subwords_in_test)

    CHUNK_LENGTH = 80

    train_dataset = LanguageModelDataset(train_token_ids,
                                         chunk_length=CHUNK_LENGTH)
    test_dataset = LanguageModelDataset(test_token_ids,
                                        chunk_length=CHUNK_LENGTH)

    torch_transf_model = LanguageModel(tokenizer.vocab_size(),
                                       256,
                                       BatchFirstTransformerEncoder(
                                           nn.TransformerEncoderLayer(
                                               d_model=256,
                                               nhead=16,
                                               dim_feedforward=512,
                                               dropout=0.1),
                                           num_layers=3),
                                       emb_dropout=0.1)

    (best_val_loss,
     best_torch_transf_model) = train_eval_loop(torch_transf_model,
                                                train_dataset,
                                                test_dataset,
                                                lm_cross_entropy,
                                                lr=2e-3,
                                                epoch_n=2000,
                                                batch_size=256,
                                                device=None,
                                                early_stopping_patience=50,
                                                max_batches_per_epoch_train=1000,
                                                max_batches_per_epoch_val=1000,
                                                lr_scheduler_ctor=lr_scheduler)

    best_torch_transf_model.save(model_weights_filename)

    return best_torch_transf_model

def generate_main(model_weights_filename, BPE_MODEL_NAME):

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    device = torch.device(device)

    tokenizer = yttm.BPE(BPE_MODEL_NAME)

    torch_transf_model = LanguageModel.load(model_weights_filename, map_location=device)
    greedy_generator = GreedyGenerator(torch_transf_model, tokenizer, device=device)

    return greedy_generator

import numpy as np
import torch, torch.nn as nn
from torch.autograd import Variable

from data_loader import DataLoader, BatchGenerator
from .simple_rnn_model import CharRNNCell

import time
from tqdm import tqdm

def train_main(text_path, save_path, num_epochs=20):

    DL = DataLoader(text_path)
    BG = BatchGenerator(DL.tensor).generate_batches(DL.n_batches, batch_size=DL.batch_size)

    char_rnn = CharRNNCell(num_tokens=DL.vocabulary_size)
    optimizer = torch.optim.Adam(char_rnn.parameters(), lr=0.01)
    criterion = nn.NLLLoss()

    for i in tqdm(range(num_epochs)):

        epoch_losses = []
        time_epoch = time.time()
        char_rnn.train()

        for j in range(DL.n_batches):
            x, y = next(BG)
            x = Variable(torch.from_numpy(x).to(torch.int64))
            y = Variable(torch.from_numpy(y).to(torch.int64)).contiguous()
            #         batch_ix = torch.tensor(batch_ix, dtype=torch.int64)

            logp_seq = char_rnn.rnn_loop(x, batch_size=DL.batch_size)

            # compute loss
            #         predictions_logp = logp_seq[:, :-1]
            #         actual_next_tokens = batch_ix[:, 1:]
            loss = criterion(logp_seq, y.view(-1))
            #         loss = -torch.mean(torch.gather(logp_seq, dim=2, index=y[:,:,None]))###YOUR CODE

            # train with backprop
            loss.backward()
            nn.utils.clip_grad_norm_(char_rnn.parameters(), 5)
            optimizer.step()
            optimizer.zero_grad()

            epoch_losses.append(float(loss))

        duration_epoch = time.time() - time_epoch
        print("epoch: %s, duration: %ds, loss: %.6g." %
              (i + 1, duration_epoch, np.mean(epoch_losses)))
        print(char_rnn.generate_sample(DL, 'Motherfucker', max_length=512, temperature=.7))

    char_rnn.save(save_path + f'/model_Vanilla_RNN.pt')

    return char_rnn

def generate_main(text_path, model_weights_path, prefix = 'Fuck', length=512, temperature=.7):

    DL = DataLoader(text_path)
    model = CharRNNCell.load(model_weights_path)

    return model.generate_sample(DL, prefix, max_length=length, temperature=temperature)
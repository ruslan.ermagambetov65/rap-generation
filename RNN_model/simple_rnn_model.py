import torch, torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import numpy as np

class CharRNNCell(nn.Module):
    """
    Implement the scheme above as torch module
    """

    def __init__(self, num_tokens, embedding_size=16, rnn_num_units=64):
        super(self.__class__, self).__init__()

        self.args = {"rnn_num_units": rnn_num_units,
                     "num_tokens": num_tokens,
                     "embedding_size": embedding_size
                     }

        self.embedding = nn.Embedding(num_tokens, embedding_size)
        self.rnn_update = nn.Linear(embedding_size + rnn_num_units, rnn_num_units)
        self.rnn_to_logits = nn.Linear(rnn_num_units, num_tokens)

    def forward(self, x, h_prev):
        """
        This method computes h_next(x, h_prev) and log P(x_next | h_next)
        We'll call it repeatedly to produce the whole sequence.

        :param x: batch of character ids, variable containing vector of int64
        :param h_prev: previous rnn hidden states, variable containing matrix [batch, rnn_num_units] of float32
        """
        # get vector embedding of x
        x_emb = self.embedding(x)

        # compute next hidden state using self.rnn_update
        x_and_h = torch.cat([x_emb, h_prev], dim=1)
        h_next = self.rnn_update(x_and_h)

        h_next = torch.tanh(h_next)

        assert h_next.size() == h_prev.size()

        # compute logits for next character probs
        logits = self.rnn_to_logits(h_next)

        return h_next, F.log_softmax(logits, -1)

    def initial_state(self, batch_size):
        """ return rnn state before it processes first input (aka h0) """
        return Variable(torch.zeros(batch_size, self.args["rnn_num_units"]))

    def save(self, checkpoint_path="model.pt"):
        """
        saves model and args to checkpoint_path.
        """
        checkpoint = {"args": self.args, "state_dict": self.state_dict()}
        torch.save(checkpoint, checkpoint_path)

    @classmethod
    def load(cls, checkpoint_path):
        """
        loads model from checkpoint_path.
        """
        with open(checkpoint_path, "rb") as f:
            checkpoint = torch.load(f)
        model = cls(**checkpoint["args"])
        model.load_state_dict(checkpoint["state_dict"])
        return model

    def rnn_loop(self, batch_x, batch_size):
        """
        Computes log P(next_character) for all time-steps in names_ix
        :param names_ix: an int32 matrix of shape [batch, time], output of to_matrix(names)
        """
        #     batch_size, max_length = batch_index.size()
        hid_state = self.initial_state(batch_size)
        logprobs = []

        for x_t in batch_x.transpose(0, 1):
            hid_state, logp_next = self.forward(x_t, hid_state)
            logprobs.append(logp_next)

        return torch.stack(logprobs, dim=1).view(-1, self.args["num_tokens"])

    def generate_sample(self, dataloader, seed_phrase=' ', max_length=512, temperature=1.0):
        '''
        The function generates text given a phrase of length at least SEQ_LENGTH.
        :param seed_phrase: prefix characters. The RNN is asked to continue the phrase
        :param max_length: maximum output length, including seed_phrase
        :param temperature: coefficient for sampling.  higher temperature produces more chaotic outputs,
                            smaller temperature converges to the single most likely output
        '''
        self.eval()

        generated = seed_phrase
        encoded = np.array(list(map(dataloader.vocabulary.get, seed_phrase)))
        encoded = Variable(torch.from_numpy(encoded)).to(torch.int64)

        x_sequence = encoded[:-1].unsqueeze(1).t()
        #     x_sequence = [token_to_id[token] for token in seed_phrase]
        #     x_sequence = torch.tensor([x_sequence], dtype=torch.int64)
        hid_state = self.initial_state(batch_size=1)

        # feed the seed phrase, if any
        for i in range(len(seed_phrase) - 1):
            hid_state, _ = self.forward(x_sequence[:, i], hid_state)

        # start generating
        next_x = x_sequence[:, -1]

        for _ in range(max_length - len(seed_phrase)):
            hid_state, logp_next = self.forward(next_x, hid_state)
            p_next = F.softmax(logp_next / temperature, dim=-1).data.numpy()[0]

            # sample next token and push it back into x_sequence
            next_ix = np.random.choice(len(p_next), p=p_next)

            generated += dataloader.id2vocabulary[next_ix]
            #         next_ix = torch.tensor([[next_ix]], dtype=torch.int64)
            #         x_sequence = torch.cat([x_sequence, next_ix], dim=1)
            next_x = torch.tensor([next_ix])

        #     return ''.join([tokens[ix] for ix in x_sequence.data.numpy()[0]])
        return generated